<?php

	require_once __DIR__ . '/vendor/autoload.php';

	use Illuminate\Database\Capsule\Manager as Database;
	
	$db = new Database();
	
	$db->addConnection([
		'driver'    => 'mysql',
		'host'      => 'prod.adjust.be',
		'database'  => 'vanin_webinars',
		'username'  => 'vanin_secondaire',
		'password'  => 'adjd27e',
		'charset'   => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix'    => '',
	]);
	
	$db->setAsGlobal();
	
	$submitted = false;
	if(!empty($_POST)) {
		
		if(is_array($_POST['interest'])) {
			$_POST['interest'] = implode(',', $_POST['interest']);
		}
		
		$db->table('subscriptions')->insert($_POST);
		
		if(!empty($_POST['email'])) {
		
			// Create the Transport
			$transport = (new Swift_SmtpTransport('smtp.eu.mailgun.org', 587))
			  ->setUsername('postmaster@mg.adj.tools')
			  ->setPassword('98c72593f57c0fae63064e33e2168ff7-e49cc42c-e143e5bf')
			;
			
			// Create the Mailer using your created Transport
			$mailer = new Swift_Mailer($transport);
			
			$html = file_get_contents(__DIR__ . '/emails/confirmation.html');
			$html = str_ireplace("*|FIRSTNAME|*", ucfirst(strtolower($_POST['firstname'])) , $html);
			
			// Create a message
			$message = (new Swift_Message('Van In Days: confirmation d\'inscription'))
			  ->setFrom(['informations@vanin.be' => 'Van in Days'])
			  ->setTo([$_POST['email'] => $_POST['firstname']. ' ' . $_POST['lastname']])
			  ->setBody($html, 'text/html');
			  ;
			
			// Send the message
			$result = $mailer->send($message);
			
			$key = 'de76037626359b55f13f2663717e5ab8';
			$product = 'vanin';
			$action = 'VanIn pre-inscriptions';
			
			$action_identifier = 'vanin-pre-inscriptions';
			$form_name = $action;
			
			// Datas
			$email = $_POST['email'];
			$lng = 'fr';
			$optin = 1;
			
			$r = \Adjust\GemstoneApi\Facade\Events::insertPostFormResult($action_identifier, $form_name, $email, $lng, $optin, $_POST);
			
		
		}
		
		$submitted = true;
	}