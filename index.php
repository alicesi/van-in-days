<?php include 'database.php'; ?>

<!doctype html>
<html class="no-js" lang="" >
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        
        <title>Van in days</title>
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="/dist/css/main.css">
            
    </head>
    <body>
  
        <header>
            <div class="sub-header">
                <div class="container">

                    <a  href="https://udiddit.vanin.be/fr" target="_blank">Découvrir Udiddit</a></div>
                </div>
            <div class="container">
                <img src="/src/img/logo_vanin_secondaire.png" alt="Logo Van in">
                <a href="https://www.vanin-secondaire.be/">Retour au site</a>
            </div>
        </header>

        <section class="hero">
            <div class="container">
                <img data-aos="zoom-out-right" class="claims_Vanin" src="/src/img/VanInDays.png" alt="Van in Days">
                <div class="text-content <?php if($submitted): ?>submit<?php endif; ?>" data-aos="fade-up" data-aos-duration="600">
                    <div class="info--event">
                        <p>Quand: 24 au 27 mars</p>
                        <p>Démarre à: 14:00</p>
                    </div>

                    <h1>Le rendez-vous incontournable pour les enseignants du secondaire !</h1>
                    <p><strong> 4 journées passionnantes</strong> avec pas moins de 11 conférences sur nos méthodes phares ainsi que 2 conférences exclusives en pédagogie.</p>
                    <div class="countdown <?php if($submitted): ?>submit-dn<?php endif; ?>"  >
                        <p>Les VAN IN DAYS commencent dans...</p>
                        <div id="countdown">
                            <ul>
                                <li data-aos="fade-up" data-aos-duration="600"><span id="days<?php if($submitted): ?>-none<?php endif; ?>"></span>Jours</li>
                                <li data-aos="fade-up" data-aos-duration="800"><span id="hours<?php if($submitted): ?>-none<?php endif; ?>"></span>Heures</li>
                                <li data-aos="fade-up" data-aos-duration="1000"><span id="minutes<?php if($submitted): ?>-none<?php endif; ?>"></span>Minutes</li>
                                <li data-aos="fade-up" data-aos-duration="1200"><span id="seconds<?php if($submitted): ?>-none<?php endif; ?>"></span>Secondes</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="content--register" data-aos="fade-left"
                data-aos-anchor="#example-anchor"
                data-aos-offset="500"
                data-aos-duration="1000">
                    <div class="content-video <?php if($submitted): ?>submit_reponsive<?php endif; ?>">
                        <img src="./src/img/van-in_video02.gif" alt="Formulaire s'enregistrer">
                    </div>
                    <div>
                    <?php if($submitted): ?>
                        <div class="countdown <?php if($submitted): ?>submit<?php endif; ?>">
                            <p>Merci pour votre inscription.<br><br>Surveillez votre boîte mail dans les jours à venir, vous allez recevoir un e-mail pour réserver votre place pour les différentes sessions.<br><br>Les VAN IN DAYS démarrent dans :</p>
                          
                            <div id="countdown">
                                <ul>
                                    <li ><span id="days"></span>Jours</li>
                                    <li><span id="hours"></span>Heures</li>
                                    <li><span id="minutes"></span>Minutes</li>
                                    <li><span id="seconds"></span>Secondes</li>
                                </ul>
                            </div>
                        </div>
                     <?php endif; ?>
                        <form action="" method="post" id="init_form"  class="<?php if($submitted): ?>submit-dn<?php endif; ?>">
                            <label for="lastname" required>Adresse e-mail</label>
                            <input type="email" id="email" name="email" required>
                            <button class="btn_subscribe">S’inscrire</button>
                        </form>
                    </div>   
                </div>

            </div>
        </section>

        <section class="why_sub">
            <h2>Pourquoi participer ?</h2>
            <div class="container">
                <div class="block--why" data-aos="fade-up" data-aos-duration="400" >
                    <div><img src="./src/img/ouvrage_img.jpg" alt="Découvrez nos ouvrages"></div>
                    <div class="content--why" > 
                        <h3>Découvrez nos ouvrages</h3>
                        <p>Assistez en avant-première à la présentation de nos nouvelles collections et re-découvrez nos méthodes phares</p>
                    </div>
                </div>
                <div class="block--why" data-aos="fade-up" data-aos-duration="500">
                    <div><img src="./src/img/question_img.jpg" alt="Posez vos questions en direct"></div>
                    <div class="content--why" > 
                        <h3>Posez vos questions en direct</h3>
                        <p>Toute notre équipe se tient prête à vous aider 'en live' pendant les VAN IN DAYS</p>
                    </div>
                </div>
                <div class="block--why" data-aos="fade-up" data-aos-duration="800">
                    <div><img src="./src/img/gift_img.jpg" alt="Recevez un ouvrage gratuit"></div>
                    <div class="content--why">
                        <h3>Recevez un ouvrage gratuit</h3>
                        <p>Participez à nos conférences et recevez l'ouvrage de votre choix parmi notre sélection</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="contact" data-aos="fade-up"
        data-aos-duration="400">
            <div class="container">
                <div class="content--contact">
                    <div class="contact--form" >
                        <h3>Vous souhaitez un contact privilégié ou une présentation personnalisée ? </h3>
                        <p>Contactez le délégué pédagogique de votre région </p>
                      <a href="https://form.jotform.com/210274081243344" target="_blank" class="cta">CONTACTER VOTRE DÉLÉGUÉ</a>
                    </div>
                    <div>
                        <img src="./src/img/form-home_img.jpg" alt="Image formulaire contact">
                    </div>
                </div>
            </div>
        </section>

        <footer class="container">

            <div class="content--footer">
                <div>
                    <img src="./src/img/logovanin.png" alt="Logo Van in">
                </div>
                <div class="content-link">
                    <ul>
                        <li><a href="https://www.vanin-secondaire.be/cookies/" target="_blank">Cookies</a></li>
                        <li><a href="https://www.vanin-secondaire.be/privacy/" target="_blank">Privacy</a></li>
                        <li><a href="https://www.vanin-secondaire.be/disclaimer/" target="_blank">Disclaimer</a></li>  
                    </ul>
                </div>
            </div>

        </footer>

        <div class="modal_form" data-aos="fade-in">
            <span class="modal_close">Fermer</span>
                <div class="form--content container">
                    <div class="content-bg">
                        <img class="claims_Vanin" src="/src/img/VanInDays.png" alt="Van in Days">
                    </div>
                    <div class="content--modal">
                        <h3>Formulaire d'inscription</h3>
                        <p>Inscrivez-vous et participez aux VAN IN DAYS !</p>
                        <form action="?action=submitted" method="post" id="contact_form" >
                            <div class="third">
                                <label for="lastname">Nom</label>
                                <input required type="text" id="lastname" name="lastname">
                            </div>
                                
                            <div class="third">
                                <label for="firstname">Prénom</label>
                                <input required type="text" id="firstname" name="firstname">
                            </div>
                            <div class="third">
                                <label for="email">Adresse e-mail</label>
                                <input required type="email" id="email" name="email">
                            </div>
                            
                            <div class="choice--fonction" required>
                                <p>Quelle est votre fonction ? - Plusieurs choix possibles</p>
                                <div class="checkbox">
                                    <label for="Enseignant">Enseignant(e)</label>
                                    <input   type="checkbox" id="Enseignant" name="interest[]" value="Enseignant" checked>
                                </div>
                                <div class="checkbox">
                                    <label for="directeur">Directeur(trice)</label>
                                    <input   type="checkbox" id="directeur" name="interest[]" value="directeur">
                                </div>
                                <div class="checkbox">
                                    <label for="Coordinateur">Coordinateur(trice)</label>
                                    <input  type="checkbox" id="Coordinateur" name="interest[]" value="Coordinateur">
                                </div>
                                <div class="checkbox">
                                    <label for="Autre">Autre</label>
                                    <input  type="checkbox" id="Autre" name="interest[]" value="Autre">
                                </div>
                            </div>
                            
                            <button class="btn_subscribe">S’inscrire</button>
                        </form>
                    </div>
                </div>
            </div>
        <script
          src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
          integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
          crossorigin="anonymous"></script>
     
          <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="/dist/js/main.js"></script>
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178651959-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-178651959-1');
        </script>
        <?php if($submitted): ?>
        <script>
            gtag('event', 'subscription', {
                event_category: "form",
                event_action: "submitted"
            });
        </script>
        <?php endif; ?>
    </body>
</html>