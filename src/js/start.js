$(document).ready(() => {
	
	$('#init_form').submit(function(){
		
		// Copy field to modal form
		$(this).find('input').each(function(){
			var inputName = $(this).attr('name');
			if(inputName.length > 0 ) {
				$('#contact_form').find('input[name=' + inputName + ']').val($(this).val());
			}
		});
		
		$( ".modal_form" ).show( "slow" );
		$( "body" ).addClass( "fixed" );
		return false;
	});
	
	(function () {
		const second = 1000,
			minute = second * 60,
			hour = minute * 60,
			day = hour * 24;
	  
		let birthday = "March 24, 2021 14:00:00",
			countDown = new Date(birthday).getTime(),
			x = setInterval(function() {    
	  
			  let now = new Date().getTime(),
				  distance = countDown - now;
	  
			  	document.getElementById("days").innerText = Math.floor(distance / (day)),
				document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
				document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
				document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);
	  
			  //do something later when date is reached
			  if (distance < 0) {
				let headline = document.getElementById("headline"),
					countdown = document.getElementById("countdown"),
					content = document.getElementById("content");
	  
				headline.innerText = "It's my birthday!";
				countdown.style.display = "none";
				content.style.display = "block";
	  
				clearInterval(x);
			  }
			  //seconds
		}, 0)
	}());

	$( ".modal_close" ).click(function() {
		$( ".modal_form" ).hide( "slow" );
		$( "body" ).removeClass( "fixed" );
	});

	$( ".btn_subscribe" ).click(function() {
		// Moved in form submit ^^
		//$( ".modal_form" ).show( "slow" );
	});

	AOS.init();
})
